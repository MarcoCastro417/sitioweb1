// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD4a4XAtTgp9kIroKUp8UDHrla2LTSdKUQ",
    authDomain: "sitiowebmarcocastro.firebaseapp.com",
    databaseURL: "https://sitiowebmarcocastro-default-rtdb.firebaseio.com",
    projectId: "sitiowebmarcocastro",
    storageBucket: "sitiowebmarcocastro.appspot.com",
    messagingSenderId: "122481636297",
    appId: "1:122481636297:web:1b6f8d30726c32c136ae18"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

var login = document.getElementById("ingresar");
var usuario = "";
var contrasena = "";
login.addEventListener('click', comprobarUsuario);

function extraerInfo() {
    usuario = document.getElementById("usuario").value;
    contrasena = document.getElementById("contrasena").value;
}

function comprobarUsuario() {
    extraerInfo();
    const dbref = ref(db);
    get(child(dbref, 'usuarios/' + usuario))
    .then((snapshot) => {
        if (snapshot.exists()) {
            if (contrasena == snapshot.val().contrasena) {
                alert("Inició sesión, bienvenido " + usuario)
                window.open("/html/admin.html", "_self")
            }
            else {
                alert("Contraseña incorrecta, vuelva a intentarlo")
                window.open("/html/error.html", "_self")
            }
        }
        else {
            alert("No existe el usuario");
        }
    }).catch((error) => {
        alert("error comprobarUsuario" + error);
    });
}
