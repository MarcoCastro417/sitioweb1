// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD4a4XAtTgp9kIroKUp8UDHrla2LTSdKUQ",
    authDomain: "sitiowebmarcocastro.firebaseapp.com",
    databaseURL: "https://sitiowebmarcocastro-default-rtdb.firebaseio.com",
    projectId: "sitiowebmarcocastro",
    storageBucket: "sitiowebmarcocastro.appspot.com",
    messagingSenderId: "122481636297",
    appId: "1:122481636297:web:1b6f8d30726c32c136ae18"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const storage = getStorage();

// login
var usuario = "";
var contrasena = "";

// administrador
var codigo = "";
var nombre = "";
var descripcion = "";
var precio = "";
var imgNombre = "";
var url = "";
var estatus = "";

// Botones

var btnGuardar = document.getElementById('btnGuardar');
var btnActualizar = document.getElementById('btnActualizar');
var btnConsultar = document.getElementById('btnConsultar');
var btnDeshabilitar = document.getElementById('btnDeshabilitar');
var btnLimpiar = document.getElementById('btnLimpiar');
var verImagen = document.getElementById('verImagen');
var btnTodos = document.getElementById("btnTodos");
var lista = document.getElementById("lista");
var url = document.getElementById("url");
var img = document.getElementById("img");

//guardarProducto

function guardarProducto() {
    extraerInfo();
    if (codigo == "" || nombre == "" || precio == "" || url == "") {
        alert("Dejó campos vacíos, vuelva a intentarlo...")
    } else {
        set(ref(db, 'productos/' + codigo), {
            nombre: nombre,
            descripcion: descripcion,
            precio: precio,
            imgNombre: imgNombre,
            url: url,
            estatus: estatus
        }).then((resp) => {
            alert("Se realizó el registro correctamente :D");
            limpiarCampos();
        }).catch((error) => {
            alert("Surgió un error: " + error);
        })
    }
    console.log(estatus);
}

function actualizarProducto() {
    extraerInfo();
    update(ref(db, 'productos/' + codigo), {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        imgNombre: imgNombre,
        url: url,
        estatus: estatus
    }).then(() => {
        alert("Se realizó la actualización");
        limpiarCampos();
    }).catch(() => {
        alert("Ocurrió un error :C " + error);
    });
}

function consultarProducto() {
    extraerInfo();
    const dbref = ref(db);
    get
        (child(dbref, 'productos/' + codigo))
        .then((snapshot) => {
            if (snapshot.exists()) {
                nombre = snapshot.val().nombre;
                descripcion = snapshot.val().descripcion;
                precio = snapshot.val().precio;
                imgNombre = snapshot.val().imgNombre;
                url = snapshot.val().url;
                estatus = snapshot.val().estatus;
                llamarInformacion();
                descargarImagen();
            }
            else {
                alert("No existe el registro solicitado :c");
            }
        }).catch((error) => {
            alert("Surgió un error: " + error);
        })
}

function deshabilitarProducto() {
    if (codigo == "") {
        alert("No hay producto que deshabilitar");
    }
    else {
        update(ref(db, 'productos/' + codigo), {
            estatus: "1"
        }).then(() => {
            alert("El producto " + codigo + " ha sido deshabilitado");
            limpiarCampos();
        }).catch(() => {
            alert("Surgio un error" + error);
        });
    }
}

// cargarImagen

async function imagenProducto() {
    const file = event.target.files[0];
    const name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
    const storage = getStorage();
    const storageRef = refS(storage, 'imagenes/' + name);
    uploadBytes(storageRef, file).then((snapshot) => {
        descargarImagen(name);
        alert("Se subió la imagen")

    });
}

// descargarImagen

async function descargarImagen(name) {

    img = document.getElementById('imgNombre').value;
    alert("el archivo es:" + 'imagenes/' + img)
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/' + img);
    // Get the download URL
    getDownloadURL(starsRef)
        .then((url) => {
            // Insert url into an <img> tag to "download"
            document.getElementById('imagen').src = url;
            console.log(url);
            document.getElementById('img').src = url;
            document.getElementById('url').value = url;
        })
        .catch((error) => {
            switch (error.code) {
                case 'storage/object-not-found':
                    console.log("No se encontró la imagen")
                    break;
                case 'storage/unauthorized':
                    console.log("No tiene los permisos para accesar a la imagen")
                    break;
                case 'storage/canceled':
                    console.log("Se canceló la subida");
                    break;
                // ...
                case 'storage/unknown':
                    break;
            }
        });
}

// Extraer información

function extraerInfo() {
    codigo = document.getElementById('codigo').value;
    nombre = document.getElementById('nombre').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    url = document.getElementById('url').value;
    imgNombre = document.getElementById('imgNombre').value;
    estatus = document.getElementById('estatus').value;
}

// Llamar información

function llamarInformacion() {
    document.getElementById('codigo').value = codigo;
    document.getElementById('nombre').value = nombre;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('estatus').value = estatus;
    document.getElementById('url').value = url;
    document.getElementById('imgNombre').value = imgNombre;
}

// Limpiar

function limpiarCampos() {
    document.getElementById('codigo').value = "";
    document.getElementById('nombre').value = "";
    document.getElementById('descripcion').value = "";
    document.getElementById('precio').value = "";
    document.getElementById('img').value = "";
    document.getElementById('url').value = "";
    document.getElementById('imagen').src = "/img/logo_techi.png";
    document.getElementById('imgNombre').value = "";
    document.getElementById('estatus').value = "-1";
}

// codificacion de eventos click de los botones

btnGuardar.addEventListener('click', guardarProducto);
btnActualizar.addEventListener('click', actualizarProducto);
btnConsultar.addEventListener('click', consultarProducto);
btnDeshabilitar.addEventListener('click', deshabilitarProducto);
btnLimpiar.addEventListener('click', limpiarCampos);
// verImagen.addEventListener('click', descargarImagen);
img = addEventListener('change', imagenProducto);