// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import { getDatabase, onValue, ref, set, child, get, update, remove } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyD4a4XAtTgp9kIroKUp8UDHrla2LTSdKUQ",
    authDomain: "sitiowebmarcocastro.firebaseapp.com",
    databaseURL: "https://sitiowebmarcocastro-default-rtdb.firebaseio.com",
    projectId: "sitiowebmarcocastro",
    storageBucket: "sitiowebmarcocastro.appspot.com",
    messagingSenderId: "122481636297",
    appId: "1:122481636297:web:1b6f8d30726c32c136ae18"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var producto = document.getElementById('productos');

function extraerProductos() {
    const db = getDatabase();
    const dbRef = ref(db, 'productos/');
    onValue(dbRef, (snapshot) => {
        producto.innerHTML = "";
        snapshot.forEach((childSnapshot) => {
            const childKey = childSnapshot.key;
            const childData = childSnapshot.val();
            if (childData.estatus == "0") {
                producto.innerHTML = producto.innerHTML + "<tr>" + "<td>" +
                    "<p>" + childData.nombre + 
                    "<img src='" + childData.url + "' alt=''>" +
                    "<p>" + "$" + childData.precio + "<br>" + "Descripción: " + childData.descripcion + "</p>" +
                    "</td>" + "</tr>";
            }
        });
        {
            onlyOnce: true
        }
    });
}
window.onload(extraerProductos());